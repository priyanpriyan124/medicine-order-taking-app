/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  student
 * Created: 08-Apr-2022
 */

CREATE DATABASE MEDICINE_ORDER_TAKING_APP;
USE MEDICINE_ORDER_TAKING_APP;
CREATE TABLE STORE_DETAILS(ID INT PRIMARY KEY AUTO_INCREMENT,MEDICALSTORE_NAME VARCHAR(20)NOT NULL,ADDRESS VARCHAR(20) NOT NULL,PHONE_NUMBER VARCHAR(20) NOT NULL);
DESC STORE_DETAILS;
CREATE TABLE MEDICINE_DETAILS(ID INT PRIMARY KEY,MANUFACTURE_NAME VARCHAR(20) NOT NULL,MEDICINE_NAME VARCHAR(20) NOT NULL);
DESC MEDICINE_DETAILS;
CREATE TABLE ORDER_TAKEN(MEDICAL_STORE_ID INT FOREIGN KEY,MEDICAL_STORE_NAME VARCHAR(20) NOT NULL,MEDICINE_NAME VARCHAR(20) NOT NULL,QUANTITY DECIMAL(8,2),UNIT VARCHAR(20) NOT NULL,TOTAL DECIMAL(8,2)); 
DESC ORDER_TAKEN;